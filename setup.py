from setuptools import setup

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='falchemy',
      version='0.1',
      description=' REST API development project using Falcon and SqlAlchemy ',
      long_description=readme(),
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6'
      ],
      keywords='Falcon REST Python3 SqlAlchemy ',
      url='https://bitbucket.org/morfat/falchemy',
      author='Morfat Mosoti',
      author_email='morfatmosoti@gmail.com',
      license='MIT',
      packages=['falchemy','falchemy.auth','falchemy.conf'],
      install_requires=[
         'gunicorn', 'SQLAlchemy','mysqlclient','serpy','falcon','jwcrypto'
      ],
      include_package_data=True,
      zip_safe=False)