import uuid
import datetime

from argon2 import PasswordHasher as ph
import argon2 
import random

import xml.etree.ElementTree as ET


def is_valid_password(hashed,raw_password):
    try:
        return ph().verify(hashed,raw_password)
    except argon2.exceptions.VerifyMismatchError:
        return False

def hash_password(raw_password):
    return ph().hash(raw_password)

def generate_random_raw_password():
    return str( random.randint(100000,999999) )

def generate_random_hashed_password():
    raw_password = generate_random_raw_password()
    return hash_password( raw_password )
    
    
def timestamp_uuid():
    return ( str( datetime.datetime.utcnow().timestamp() ).split('.')[0] ) + uuid.uuid4().hex

def convert_xml_to_dict(xml):
    root=ET.fromstring(xml.decode())
    it=root.iter()

    d={}
    for i in it:
        if len(i)==0:
            tag=i.tag.strip()
            if '}' in tag:
                tag=(tag.split('}')[1]).strip()
            text=i.text.strip()

            d.update({tag:text})

    return d