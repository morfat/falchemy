
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.ext.declarative import has_inherited_table

from sqlalchemy import Column, Integer, String, Boolean,DateTime,Date,ForeignKey,Numeric,UniqueConstraint, Float
from falchemy.utils import timestamp_uuid

import datetime

def CharField(max_length,*args,**kwargs):
    return Column(String(max_length),*args,**kwargs)

def DateTimeField(*args,**kwargs):
    return Column(DateTime,*args,**kwargs)

def DateField(*args,**kwargs):
    return Column(Date,*args,**kwargs)
 

def BooleanField(*args,**kwargs):
    return Column(Boolean,*args,**kwargs)

def IntegerField(*args,**kwargs):
    return Column(Integer,*args,**kwargs)

def ForeignKeyField(references,*args,**kwargs):
    return CharField(50,ForeignKey(references),*args, **kwargs)

def DecimalField(max_digits,decimal_places,*args,**kwargs):
    return Column(Numeric(precision = max_digits, scale = decimal_places),*args,**kwargs)

def FloatField(*args,**kwargs):
    return Column(Float,*args,**kwargs)

def UniqueTogether(*args,**kwargs):
    return UniqueConstraint(*args,**kwargs)

def PrimaryKeyField():
    return CharField(50, primary_key=True, default=timestamp_uuid)



class BaseTable:
    id = PrimaryKeyField() 
    
class TimestampMixin:
    created_at = DateTimeField(nullable = False,default = datetime.datetime.utcnow)
    updated_at = DateTimeField(nullable = False,default = datetime.datetime.utcnow,onupdate = datetime.datetime.utcnow)
    
Base = declarative_base(cls = BaseTable)