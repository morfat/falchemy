from sqlalchemy import select, insert, update, delete
from sqlalchemy.sql import and_, or_
from sqlalchemy import column, literal_column
from sqlalchemy import exc

import falcon

from falchemy.paginators import CursorPaginator
from falchemy.conf.settings import settings

class SqlResourceStore:

    """ Base class for all resource classes """

    table = None
    selected_columns = None
    queryset = None

    req_serializer_class = None
    resp_serializer_class = None
    pk_column_name = 'id'




    def get_queryset(self, *args, **kwargs):
        if self.queryset is not None:
            return self.queryset

        elif self.selected_columns is not None:
            return select( self.selected_columns )
        
        return select( [ self.table ] )
    

    def __get_column_filter(self,col_name,col_condition,col_value):
        #example input => ('name','contains','mosoti',)

        col = literal_column( col_name ) #to sqlalchemy column

        ops_dict = {
            'startswith':col.startswith(col_value),
            'endswith': col.endswith(col_value),
            'contains': col.contains(col_value),
            'eq': col.op("=")(col_value),
            'lt': col.op("<")(col_value),
            'lte': col.op("<=")(col_value),
            'gt': col.op(">")(col_value),
            'gte': col.op(">=")(col_value)
        }   
    
        return ops_dict[col_condition]
    

    def __get_col_filters(self, **filters):
        """ for the filters params we expect example dict:
           { 'name__startswith': ' mosoti' , 'age': 20, 'gender__ne':'M' } e.t.c.
        """

        col_filters = []

        for field_and_condition, field_value in filters.items():
            field__condition = field_and_condition.split("__")
            
            field_name = field__condition[0]
            condition = 'eq' #by default we expect this

            try:
                condition = field__condition[1]
            except IndexError:
                pass
            
            col_filters.append( self.__get_column_filter(field_name , condition ,field_value) )
        return col_filters

 

    def filter_queryset(self, queryset, search_by=None, filter_by=None, order_by=None):
        #print (search_by, filter_by)
        try:
            search_by = dict(search_by)
        except TypeError:
            search_by = {}
        
        try:
            filter_by = dict(filter_by)
        except TypeError:
            filter_by = {}

        #1. Apply filters
        return queryset.where(
            and_(
                or_(
                    *self.__get_col_filters(**search_by)
                ),
                *self.__get_col_filters(**filter_by)
            )
        )
    
    def execute_queryset(self, conn, queryset, *args, **kwargs):
        return conn.execute(queryset)
    
    def fetch_all(self, conn, queryset, limit=None, for_update=False):
        """ executed db. and gets results """
        if limit:
            queryset = queryset.limit(limit)
        
        if for_update:
            queryset = queryset.with_for_update()
        
        return self.execute_queryset(conn,queryset).fetchall()
    
    def fetch_one(self, conn, queryset, for_update=False):
        """ executed db. and gets results """
        
        if for_update:
            queryset = queryset.with_for_update()
        
        return self.execute_queryset(conn,queryset).fetchone()

    
    def find(self, filters, order_by=None):

        """ 
        find in the resource queryset . mostsly used in code or from other classes calls.
        Returns queryset
        """

        queryset = self.get_queryset()

        return self.filter_queryset(queryset, filter_by=filters, order_by=order_by)
    
    def find_and_fetch_all(self,conn, filters, order_by=None, limit=None, for_update=False, dict_results=False):
        filtered_queryset =  self.find(filters,order_by)
        results = self.fetch_all(conn, filtered_queryset,limit,for_update)
        if dict_results:
            return [ dict(r) for r in results ]
        return results

    
    def find_and_fetch_one(self,conn, filters, for_update=False, as_dict=True):
        filtered_queryset =  self.find(filters)
        result = self.fetch_one(conn, filtered_queryset,for_update=for_update)
        try:
            return dict(result)
        except TypeError:
            return None

  
    def insert_one(self,conn, data, table=None, *args, **kwargs):
     

        if table is None:
            table = self.table
        
        queryset = insert( table ).values(data)

        inserted = self.execute_queryset(conn, queryset, *args, **kwargs )
        
        
        pk_column = self.pk_column_name.split('.')[0] 

        try:
            #default column name can be in form of 'table.column'. so we try getting just the col
            pk_column = self.pk_column_name.split('.')[1]
        except IndexError:
            pass

           

        return { pk_column: inserted.inserted_primary_key[0], **data }
    
    def alter_one(self, conn, pk, new_data , extra_filters = None,  table=None, *args, **kwargs):
        #NB. never update the default dict, as it will have different data on subsequent calls

        #print (self.table.id.name, self.table.id )
        
        if table is None:
            table = self.table
     
        queryset = update( table ).values( new_data ).where( table.id == pk)

        filtered_queryset = self.filter_queryset(queryset, filter_by=extra_filters)

        #print (filtered_queryset)

        altered = self.execute_queryset(conn, filtered_queryset, *args, **kwargs )
        return altered
    
    def alter(self, conn, filters, new_data , table=None, *args, **kwargs):
       
        if table is None:
            table = self.table
     
        queryset = update( table ).values( new_data )
        if bool(filters):
            filtered_queryset = self.filter_queryset(queryset, filter_by=filters)
            #print ("FILTERS EXIST", filtered_queryset)
        else:
            print("Empty filter record given")

        altered = self.execute_queryset(conn, filtered_queryset, *args, **kwargs )
        return altered
        

    
    def delete_one(self, conn, pk, extra_filters = None,  table=None, *args, **kwargs):
        if table is None:
            table = self.table
        
        try:
            extra_filters.update({ self.pk_column_name: pk })
        except AttributeError:
            extra_filters = { self.pk_column_name: pk }

        queryset = delete( table )

        filtered_queryset = self.filter_queryset(queryset, filter_by=extra_filters)

        #print (filtered_queryset)

        deleted = self.execute_queryset(conn, filtered_queryset, *args, **kwargs )
        return deleted


class Resource(SqlResourceStore):

    """ to handle creating and listing of resoruces """

    searchable_fields = None
    filterable_fields = None

    SEARCH_QUERY_PARAM_NAME = settings.SEARCH_QUERY_PARAM_NAME
    paginator_object = CursorPaginator()
    MESSAGES = { "ON_DELETE":"Deleted", "ON_PUT": "Updated", "ON_POST": "Created" } # to display to ui


        
    def get_valid_search_params(self, search_text):
        search_params = {}
        if search_text:
            for field in self.searchable_fields:
                field__condition = "{field}__contains".format(field = field)
                search_params.update({ field__condition: search_text })
        
        return search_params
    
    def get_valid_filter_params(self, query_params):
        filter_params = {}

        for field_and_condition, field_value in query_params.items():
            field_name = field_and_condition.split("__")[0] #e.g get 'name' from 'name__startswith'

            try:
                if field_name in self.filterable_fields:
                    filter_params.update( {field_and_condition: field_value} )
            except TypeError:
                break
        
        return filter_params

    
    def get_serialized_resp_data(self, raw_data, *args, **kwargs ):
        #serialize if its serializer is given
        try:
            return self.resp_serializer_class(raw_data).data
        except TypeError:
            #when serializer is not found
            return raw_data

    
    def get_serialized_req_data(self, req, *args, **kwargs ):
        try:
            return self.req_serializer_class(req.media).data
        except KeyError as ke:
            raise falcon.HTTPBadRequest(title = "Missing Request Parameters", description = "{field} is a required field".format(field=ke) )
        
        return req.media

#MIXINS

class ResourceCreateMixin:

    def create(self, req, conn, serialized_data, *args, **kwargs):
        inserted = self.perfom_create(conn, serialized_data)
        return inserted
    
    def perfom_create(self, conn, data):
        try:
            return self.insert_one(conn, data = data)
        except exc.IntegrityError as error:
            error_message = str(error._message())
            error_message = error_message.split('1062')[1][3:][:-3]

            raise falcon.HTTPConflict(title= "Duplicate Record", description = error_message )

class ResourceListMixin:

    def list(self, req, conn, *args, **kwargs):
        queryset = self.get_queryset()

        query_params = req.params

        #filter by search or filters
        search_text = query_params.pop(self.SEARCH_QUERY_PARAM_NAME, None)
        search_params = self.get_valid_search_params(search_text)
        filter_params = self.get_valid_filter_params(query_params)


        return self.filter_queryset(queryset, search_by=search_params, filter_by=filter_params)



class ResourceRetrieveMixin:

    def retrieve(self,req, conn, pk, **kwargs):
        #print (self.table.id, { self.table.__table__.c.id : pk }, getattr(self.table, 'id') )

        result = self.find_and_fetch_one(conn, filters={ self.pk_column_name : pk })
        return result
    
    def retrieve_or_404(self,req, conn, pk, **kwargs):
        result = self.retrieve(req,conn,pk, **kwargs)
        return result
    
  

class ResourceDestroyMixin:

    def destroy(self,req, conn, pk, **kwargs):
        deleted = self.delete_one(conn, pk, **kwargs)
        return deleted

class ResourceUpdateMixin:

    def update(self, req, conn, pk, new_data, **kwargs):
        return self.perfom_update(req, conn, pk , new_data, **kwargs)
    
    def perfom_update(self, req, conn, pk, new_data, **kwargs):
        try:
            return self.alter_one(conn, pk, new_data)
        except exc.IntegrityError as error:
            error_message = str(error._message())
            error_message = error_message.split('1062')[1][3:][:-3]

            raise falcon.HTTPConflict(title= "Duplicate Record", description = error_message )

    


#RESOURCE Classes

class ResourceList(Resource, ResourceListMixin):
    def on_get(self, req, resp, *args, **kwargs):
       
        conn = req.context['conn']

        
        filtered_queryset = self.list(req, conn, *args, **kwargs)

        #apply pagination
        paginated = self.paginator_object.paginate_queryset(req, filtered_queryset, pk_column_name=self.pk_column_name)
   
        #fetch paginated
        resultset = self.fetch_all(conn, queryset=paginated.get("queryset"))
        results = [ dict(r) for r in resultset ]

        #get pagination object
        pagination = self.paginator_object.get_pagination(url=req.uri, results=results, before=paginated.get("cursor_before"),
                        after=paginated.get("cursor_after"), page_number=paginated.get("page_number"),
                        page_size=paginated.get("page_size"),pk_column_name=self.pk_column_name
                        )

        resp.media = { "data": results, "pagination": pagination }



class ResourceCreate(Resource, ResourceCreateMixin):
    def on_post(self,req, resp, *args, **kwargs):
        conn = req.context['conn']
        serialized_data = self.get_serialized_req_data(req)
        
        created = self.create(req, conn, serialized_data)

        resp_data = self.get_serialized_resp_data(created)

        resp.media = { "data": [ resp_data ] ,  "message": self.MESSAGES.get("ON_POST") }
    

class ResourceRetrieve(Resource, ResourceRetrieveMixin):

    def on_get(self,req, resp,pk,**kwargs):
        conn = req.context['conn']
        result = self.retrieve_or_404(req,conn,pk, **kwargs)
        resp.media = {"data": [ result ] , "message": None }


class ResourceUpdate( Resource, ResourceUpdateMixin , ResourceRetrieveMixin):
    def on_put(self,req, resp,pk,**kwargs):
        conn = req.context['conn']
        serialized_data = self.get_serialized_req_data(req)
        updated = self.update(req, conn, pk, serialized_data, **kwargs)

        #get updated
        result = self.retrieve_or_404(req,conn,pk, **kwargs)

        resp_data = self.get_serialized_resp_data(result)

        resp.media = { "data": [ resp_data], "message": self.MESSAGES.get("ON_PUT")  }


    
class ResourceDestroy(Resource, ResourceDestroyMixin):

    def on_delete(self,req, resp,pk,**kwargs):
        conn = req.context['conn']

        result = self.retrieve_or_404(req,conn,pk, **kwargs)
        
        self.destroy(req,conn,pk,**kwargs)

        #no content reply

        resp.status = falcon.HTTP_OK
        resp.media = {"data": [], "message": self.MESSAGES.get("ON_DELETE") }

#Grouped generic resource views

class ResourceListCreate(ResourceList, ResourceCreate):
    pass

class ResourceRetrieveUpdate(ResourceRetrieve, ResourceUpdate):
    pass

class ResourceRetrieveUpdateDestroy(ResourceRetrieve, ResourceUpdate, ResourceDestroy):
    pass
