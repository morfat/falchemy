
from jwcrypto import jwt, jwk,jwe
import datetime
import falcon

import json

from falchemy.conf.settings import settings

def validate_jwt_token(bearer_token):
   
    if not bearer_token:
        return None

    k = {"k": settings.JWT_SECRET_KEY, "kty":"oct"}

    key = jwk.JWK(**k)

    try:
        bearer_token_l = bearer_token.split('Bearer')
        bearer_token = bearer_token_l[1].strip()


        ET = jwt.JWT(key = key, jwt = bearer_token)
      
        ST = jwt.JWT(key=key, jwt=ET.claims) # check_claims = required_token_claims)
        return json.loads(ST.claims)
   
    except jwt.JWTExpired:
        raise falcon.HTTPUnauthorized(description = 'Access Token Expired')
    
    except jwt.JWTInvalidClaimValue:
        raise falcon.HTTPUnauthorized(description = 'Invalid Token . Valid Token Claims are needed')
    except jwe.InvalidJWEData:
        raise falcon.HTTPUnauthorized(title = "Invalid Data" ,description = 'Data Badly Encrypted. Please check key used.')


def generate_encrypted_token(claims):

    token = jwt.JWT( header = {"alg": "HS256"}, claims = claims )
    k = {"k": settings.JWT_SECRET_KEY, "kty":"oct"}

    key = jwk.JWK(**k)

    #sign token
    token.make_signed_token(key)
    #encrypt token
    etoken = jwt.JWT( header = {"alg": "A256KW", "enc": "A256CBC-HS512"},claims = token.serialize() )
    etoken.make_encrypted_token(key)
    return  etoken.serialize()
    